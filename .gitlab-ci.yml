image:
  name: hashicorp/terraform:light
  entrypoint:
    - "/usr/bin/env"
    - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Default output file for Terraform plan
variables:
  TF_ROOT: terraform
  PLAN: plan.tfplan
  JSON_PLAN_FILE: tfplan.json
  TF_IN_AUTOMATION: "true"

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/

stages:
  - validate
  - build
  - plan
  - apply
  - deploy
  - destroy

.install-curl-jq: &install-curl-jq
  - apk add --update curl jq
  - alias convert_report="jq -r '([.resource_changes[].change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"

.gitlab-tf-backend: &gitlab-tf-backend
  - cd $TF_ROOT
  - export TF_VAR_access_key=$DEV_access_key
  - export TF_VAR_secret_key=$DEV_secret_key
  - terraform --version
  - terraform init --backend-config="bucket=$BACKEND_BUCKET" --backend-config="key=terraform/$CI_PROJECT_NAME" --backend-config="region=$AWS_REGION" --backend-config="dynamodb_table=$DYNAMODB_TABLELOCK" --backend-config="workspace_key_prefix=environment"
  - terraform workspace select $CI_COMMIT_REF_NAME || terraform workspace new $CI_COMMIT_REF_NAME

download_python_libraries_layers:
  image: python:3.8
  stage: build
  services:
    - name: docker:dind
  variables:
    PYTHON: python3.8
  artifacts:
    paths:
      - terraform/layers
    expire_in: 1 day
  only:
    - staging
    - master
  script:
    # - /bin/sh -c "pip install -r requirements.txt -t python/lib/${penv}/site-packages/; exit"
    - echo ${CI_PROJECT_PATH}
    - cd terraform/layers
    - |
      for folder in */; do
        echo $folder && \
        cd $folder && \
        mkdir -pv python/lib/${PYTHON}/site-packages && \
        /bin/sh -c "pip install -r requirements.txt -t python/lib/${PYTHON}/site-packages/" && \
        du -sh python/ && \
        cd ..
      done
    - cd ../../

validate:
  stage: validate
  script:
    - *gitlab-tf-backend
    - terraform validate
    - terraform fmt -check=true
  only:
    - branches
    - merge_requests

merge review:
  stage: plan
  script:
    - *install-curl-jq
    - *gitlab-tf-backend
    - terraform plan -out=$PLAN
    - "terraform show --json $PLAN | convert_report > $JSON_PLAN_FILE"
  artifacts:
    expire_in: 1 week
    name: plan
    reports:
      terraform: $TF_ROOT/$JSON_PLAN_FILE
  only:
    - merge_requests

plan production:
  stage: plan
  script:
    - *gitlab-tf-backend
    - terraform plan
  only:
    - main
  resource_group: production

apply:
  stage: apply
  script:
    - *gitlab-tf-backend
    - terraform apply -auto-approve
  dependencies:
    - plan production
  only:
    - main
  resource_group: production
