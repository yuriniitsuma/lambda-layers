// resource "aws_lambda_layer_version" "main" {
//   filename            = data.archive_file.layer.output_path
//   layer_name          = var.layer_name
//   source_code_hash    = data.archive_file.layer.output_base64sha256
//   compatible_runtimes = var.compatible_runtimes
//   description         = var.description
// }

provider "aws" {
  region = "us-east-1"
}

module "layer" {
  source      = "./lambda-layer"
  layer_name  = "dependencies"
  source_dir  = "./layers/fastapi"
  source_type = "python"

  rsync_pattern = [
    "--exclude='**tests**'",
    "--exclude='**__pycache__**'",
    "--include=module/{a,b,c}/",
    "--include='*.py'",
    "--exclude='module/*'"
  ]
}
